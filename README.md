# releasepost

## Usage

```
GITLAB_API_PRIVATE_TOKEN=YOUR_TOKEN exe/releasepost status
```

```
Which milestone would you like to check? 12.4
Stage Label? devops::release
Searching www-gitlab-com for milestone 12.4 with labels release post, devops::release
Processing [============================================================]
┌─────────────────────────────────────────────────────────────────────┬────────────────────────────────┐
│  Release Post Merge Request                                         │  Feature Status                │
├─────────────────────────────────────────────────────────────────────┼────────────────────────────────┤
│  https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32769  │  complete                      │
│  https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32711  │  complete                      │
│  https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32685  │  complete                      │
│  https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32562  │  manual_verification_required  │
│  https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32422  │  complete                      │
│  https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32414  │  complete                      │
│  https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/32387  │  incomplete                    │
└─────────────────────────────────────────────────────────────────────┴────────────────────────────────┘
```

