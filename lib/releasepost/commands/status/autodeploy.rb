class Autodeploy
  class << self
    extend Memoist

    PROJECT = 'gitlab-org/gitlab'

    def commit_refs(project, sha, options = {})
      path = CLIENT.url_encode(project)

      # NOTE: The GitLab gem doesn't currently support this API
      # See https://github.com/NARKOZ/gitlab/pull/507
      # $api_call_count+=1
      CLIENT.get(
        "/projects/#{path}/repository/commits/#{sha}/refs",
        query: options
      )
    end

    def auto_deploy_branches(ref)
      # NOTE: We always use the production client, because staging is always
      # behind for the specified repository.
      commit_refs(PROJECT, ref, type: 'branch')
        .select { |b| b.name.match?(/^\d+-\d+-auto-deploy-\d+$/) }
    rescue ::Gitlab::Error::NotFound
      []
    end

    def production_status
      version = CLIENT.version
      revision = version.revision

      # Get the auto-deploy ref for the deployed revision
      auto_deploy_branch = auto_deploy_branches(revision).first

      {
        version: version.version,
        revision: version.revision,
        branch: auto_deploy_branch&.name || nil,
      }
    end
    memoize :production_status
  end
end
