class ReleasePostMergeRequestParser
  extend Memoist

  attr_reader :parser_failures, :issue_urls

  def initialize(project_id, merge_request_iid)
    @project_id         = project_id
    @merge_request_iid  = merge_request_iid
    @parser_failures    = 0
    @issue_urls         = []

    parse_related_issues_urls
    validate_urls
  end

  def parse_related_issues_urls
    merge_request_changes.changes.each do |change|
      next unless yaml_file?(change["new_path"])
      @issue_urls << issue_url(merge_request_changes.sha, change["new_path"])
    end

    @issue_urls.compact
  end
  memoize :parse_related_issues_urls

  def validate_urls
    @issue_urls.each do |issue_url|
      @parser_failures += 1 if issue_url.to_s.include?('epics')
      @parser_failures += 1 unless issue_url.to_s.include?('https://gitlab.com')
    end
  end

  def merge_request_changes
    CLIENT.merge_request_changes(@project_id, @merge_request_iid)
  end
  memoize :merge_request_changes

  def yaml_file?(path)
    path.end_with?('.yml')
  end

  def change_data(sha, path)
    data = CLIENT.file_contents(@project_id, path, sha)

    YAML.load(data)
  rescue Psych::SyntaxError, Gitlab::Error::NotFound
    @parser_failures += 1
  end

  def issue_url(sha, path)
    data = change_data(sha, path)
    return if data.nil?

    begin
      if data["features"]
        if data["features"]["primary"]
          data["features"]["primary"][0]["issue_url"]
        elsif data["features"]["secondary"]
         data["features"]["secondary"][0]["issue_url"]
        end
      else
        @parser_failures += 1
      end
    rescue NoMethodError, TypeError
      @parser_failures += 1
    end
  end
end
