class ImplementationIssue
  extend Memoist

  def initialize(issue_url, milestone)
    @issue_url              = issue_url
    @milestone              = milestone
    @parser_failures        = 0
    parse_issue_url
  end

  def parse_issue_url
    parts = @issue_url.split('/')
    if parts.count == 8
      @org_name, @group_name, @project_name, extra, @issue_iid = @issue_url.split('/')[3,7]
    elsif parts.count == 7
      @org_name, @project_name, extra, @issue_iid = @issue_url.split('/')[3,6]
    else
      @parser_failures += 1
    end
  end

  def project_id
    if @group_name.nil? && @project_name == 'gitlab'
      278964  # project id for gitlab
    elsif @group_name.nil? && @project_name == 'gitlab-ce'
      13083   # https://gitlab.com/gitlab-org/gitlab-foss
    elsif @group_name.nil? && @project_name == 'gitlab-foss'
      13083   # https://gitlab.com/gitlab-org/gitlab-foss
    elsif @group_name =='charts' && @project_name == 'gitlab'
      3828396 # https://gitlab.com/gitlab-org/charts/gitlab
    elsif @group_name.nil? && @project_name == 'gitlab-pages'
      734943 # https://gitlab.com/gitlab-org/gitlab-pages
    elsif @group_name.nil? && @project_name == 'gitlabktl'
      11080193 # https://gitlab.com/gitlab-org/gitlabktl
    elsif @group_name.nil? && @project_name == 'gitaly'
      2009901 # https://gitlab.com/gitlab-org/gitaly
    elsif @group_name.nil? && @project_name == 'cloud-deploy'
      15363819 # https://gitlab.com/gitlab-org/cloud-deploy
    else
      puts "Project not mapped: #{@project_name}"
      @parser_failures += 1
      return nil
    end
  end

  def related_merge_requests
    return [] if project_id.nil?

    data = []
    # related_merge_requests API not supported in Gitlab gem
    url = "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{@issue_iid}/related_merge_requests"
    response = HTTP.get(url)

    if response.code == 200
      related_merge_requests = JSON.parse(response)

      related_merge_requests.each do |related_merge_request|
        # begin
          if related_merge_request["milestone"] &&
              related_merge_request["milestone"]["title"] == @milestone &&
              related_merge_request["state"] != 'closed' &&
              related_merge_request["project_id"] != 7764 # skip if www-gitlab-com
            data << ImplementationMergeRequest.new(related_merge_request["project_id"], related_merge_request["iid"])
          end
        # rescue TypeError
        #   @parser_failures += 1
        # end
      end

      data
    else
      @parser_failures += 1
      return []
    end
  rescue JSON::ParserError
    @parser_failures += 1
  end
  memoize :related_merge_requests

  def related_merge_requests_statuses
    related_merge_requests.map{|mr| mr.status}
  end
  memoize :related_merge_requests_statuses

  def status
    # :complete, :incomplete, :manual_verification_required
    if @parser_failures > 0
      :manual_verification_required
    elsif related_merge_requests_statuses.all?(:complete)
      :complete
    elsif related_merge_requests_statuses.any?(:incomplete)
      :incomplete
    elsif related_merge_requests_statuses.any?(:manual_verification_required)
      :manual_verification_required
    else
      :manual_verification_required
    end
  end
end
