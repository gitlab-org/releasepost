class ImplementationMergeRequest
  extend Memoist

  def initialize(project_id, merge_request_iid)
    @project_id         = project_id
    @merge_request_iid  = merge_request_iid
  end

  def merge_request
    CLIENT.merge_request(@project_id, @merge_request_iid)
  end
  memoize :merge_request

  def sha
    merge_request.merge_commit_sha
  end

  def can_determine_status_via_autodeploy?
    merge_request.project_id == 278964 # if project gitlab-org/gitlab
  end

  def status
    # :complete, :incomplete, :manual_verification_required
    if can_determine_status_via_autodeploy?
      if ['merged', 'closed'].include?(merge_request.state)
        in_latest_autodeploy? ? :complete : :incomplete
      else
        :incomplete
      end
    else
      :manual_verification_required
    end
  end

  def in_latest_autodeploy?
    auto_deploy_branches = Autodeploy.auto_deploy_branches(sha)
    return auto_deploy_branches.any? { |b| Autodeploy.production_status[:branch] == b.name }
  end
end
