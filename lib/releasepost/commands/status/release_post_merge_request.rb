class ReleasePostMergeRequest
  extend Memoist

  def initialize(project_id, merge_request_iid, milestone)
    @project_id         = project_id
    @merge_request_iid  = merge_request_iid
    @milestone          = milestone
    @parser             = ReleasePostMergeRequestParser.new(
      @project_id,
      merge_request_iid
    )
  end

  def url
    "https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/#{@merge_request_iid}"
  end

  def status
    # :complete, :incomplete, :manual_verification_required
    if @parser.parser_failures > 0
      :manual_verification_required
    elsif implementation_issues_statuses.all?(:complete)
      :complete
    elsif implementation_issues_statuses.any?(:incomplete)
      :incomplete
    elsif implementation_issues_statuses.any?(:manual_verification_required)
      :manual_verification_required
    else
      :manual_verification_required
    end
  end

  def implementation_issues_statuses
    implementation_issues.map{|mr| mr.status}
  end
  memoize :implementation_issues_statuses

  def implementation_issues
    issues = []
    @parser.issue_urls.each do |issue_url|
      issues << ImplementationIssue.new(issue_url, @milestone)
    end
    issues
  end
  memoize :implementation_issues
end
