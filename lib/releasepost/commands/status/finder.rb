# frozen_string_literal: true

require 'memoist'
require 'gitlab'
require 'http'
require 'tty-progressbar'
require 'tty-table'
require_relative 'autodeploy'
require_relative 'release_post_merge_request'
require_relative 'release_post_merge_request_parser'
require_relative 'implementation_issue'
require_relative 'implementation_merge_request'

CLIENT = Gitlab.client(endpoint: 'https://gitlab.com/api/v4')

class Finder
  extend Memoist

  attr_reader :milestone, :labels, :project_id

  def initialize(milestone, labels = '', project_id = 7764)
    @milestone        = milestone
    @labels           = "release post, #{labels}"
    @project_id       = project_id
    @release_post_mrs = []
  end

  def start_progress_bar(total)
    @progress_bar = TTY::ProgressBar.new(
      "Processing [:bar]",
      total: total,
      width: 60
    )
  end

  def release_post_merge_requests
    search = CLIENT.merge_requests(@project_id, labels: @labels, milestone: @milestone)
    merge_requests = search.auto_paginate
    start_progress_bar(merge_requests.count)
    merge_requests
  end

  def run!
    release_post_merge_requests.each do |release_post_merge_request|
      next if release_post_merge_request.state == 'closed'

      @release_post_mrs << ReleasePostMergeRequest.new(
        @project_id,
        release_post_merge_request.iid,
        @milestone
      )
    end

    render_output
  end

  def render_output
    rows = []

    @release_post_mrs.each do |mr|
      @progress_bar.advance(1)
      rows << [mr.url, mr.status]
    end

    table = TTY::Table.new(['Release Post Merge Request', 'Feature Status'], rows)

    puts table.render(:unicode, padding: [0,2])
  end
end
