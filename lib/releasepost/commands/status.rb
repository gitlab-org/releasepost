# frozen_string_literal: true

require_relative '../command'
require_relative 'status/finder'

module Releasepost
  module Commands
    class Status < Releasepost::Command
      def initialize(options)
        @options = options
      end

      def execute(input: $stdin, output: $stdout)
        require 'tty-prompt'
        prompt = TTY::Prompt.new
        milestone = prompt.ask('Which milestone would you like to check?') do |q|
          q.validate /^\d{2}\.\d{1,2}$/
        end

        stage_label = prompt.ask('Stage Label?', default: 'group::progressive delivery')

        f=Finder.new(milestone, stage_label)

        prompt.ok("Searching www-gitlab-com for milestone #{f.milestone} with labels #{f.labels}")

        f.run!
      end
    end
  end
end
