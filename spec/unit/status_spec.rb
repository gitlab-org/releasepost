require 'releasepost/commands/status'

RSpec.describe Releasepost::Commands::Status do
  it "executes `status` command successfully" do
    output = StringIO.new
    options = {}
    command = Releasepost::Commands::Status.new(options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end
end
