RSpec.describe "`releasepost status` command", type: :cli do
  it "executes `releasepost help status` command successfully" do
    output = `releasepost help status`
    expected_output = <<-OUT
Usage:
  releasepost status

Options:
  -h, [--help], [--no-help]  # Display usage information

Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
